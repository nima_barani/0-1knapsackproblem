# Solve 0/1 Knapsack Problem by Hybrid Genetic Algorithm and Local Search

This project is an approach to solve the famous 0/1 Knapsack problem by hybrid genetic algorithm and local search. We solve this problem by A-Star algorith in order to determine how fast and accurate the proposed approach is.

## Running the tests

The program, first, reads the algorithm's parameters from a text file, "inputX.txt". 

There are 3 datasets (ks_20_878, ks_100_997, ks_200_1008); Each dataset contains two column: one represents item's value , the other one represents item's weight. The first row of each dataset represents the number of items- M- and maximum weight.

After completing the calculations, the program generates a log file which contains:
* the best individual in the population,
* the best individual's fitness value,
* the average population's fitness value,
* and the number of time the fitness function is called.
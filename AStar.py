from item import Item
import random
import numpy as np


ITEMS = []
MAX_WEIGHT = 997
OPTIMIZED_VALUE = 1634
def read_file():
    a,b = np.loadtxt("/Dataset/ks_20_878", dtype=int, unpack=True)
    values = np.array(a).tolist()
    weights = np.array(b).tolist()
    values.pop(0)
    global MAX_WEIGHT
    MAX_WEIGHT = weights.pop(0)
    for v, w in zip(values, weights):
        ITEMS.append(Item(v,w))

def start_node_initilize():
    return [random.randint(0,0) for x in range (0,len(ITEMS))]

def fScore(node):
    total_weight = 0
    total_value = 0
    index = 0
    for i in node:        
        if index >= len(ITEMS):
            break
        if (i == 1):
            total_value += ITEMS[index].value
            total_weight += ITEMS[index].weight
        index += 1
    hScore = abs(total_value - OPTIMIZED_VALUE) 
    return hScore + total_weight
    
def knapsack_value(node):
    total_value = 0
    index = 0
    for i in node:        
        if index >= len(ITEMS):
            break
        if (i == 1):
            total_value += ITEMS[index].value
        index += 1
    return total_value


def actual_cost(node):
    total_weight = 0
    index = 0
    for i in node:        
        if index >= len(ITEMS):
            break
        if (i == 1):
            total_weight += ITEMS[index].weight
        index += 1
    return total_weight

def goal_test(node):
    if knapsack_value(node) == OPTIMIZED_VALUE and actual_cost(node) <= MAX_WEIGHT:
        return True
    else:
        return False

def neighborhood(node):
    neighbors = []
    value = knapsack_value(node)
    weight = actual_cost(node)
    for idx, val in enumerate(node):
        total_value = value
        total_weight = weight
        neighbor = list(node)
        if val == 1:
            val = 0
            total_value -= ITEMS[idx].value
            total_weight -= ITEMS[idx].weight

        else:
            val = 1
            total_value += ITEMS[idx].value
            total_weight += ITEMS[idx].weight
        if total_value <= OPTIMIZED_VALUE and total_weight <= MAX_WEIGHT:
            neighbor[idx] = val
            neighbors.append(neighbor)
    return neighbors

def main():
    read_file()
    frontierSet = []
    exploredSet = []
    startNode = start_node_initilize()
    current = startNode
    nima = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 0, 1, 0, 1, 1]
    while len(frontierSet) > -1:
        # index += 1
        # print(index)
        neighbors = neighborhood(current)
        for idx, val in enumerate(neighbors):
            if val in exploredSet:
                continue
            if val not in frontierSet:
                frontierSet.append(val)
            else:
                continue
        current = min(frontierSet, key= lambda x: fScore(x))
        print(current)
        if goal_test(current):
            print(current)
            return
        frontierSet.remove(current)
        exploredSet.append(current)
    print(current)
if __name__ == "__main__":
    main()


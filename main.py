import random
from item import Item
import matplotlib.pyplot as plt
import numpy as np
from numpy import ndarray
import random

##########################################################################################
# CONFIG
ITEMS = []
INDIV_SIZE = 0
POP_SIZE = 0
MAX_WEIGHT = 0
PARENT_PERCENT = 0
OFFSPRING_PERCENT = 0
GEN_MAX = 0
CROSSOVER_PROB = 0
MUTATION_PROB = 0
TABU_SIZE = 0
MAX_SIDEWAY = 0
MAX_IMPROVE = 0
START_POP_WITH_ZEROES = True
INDEX = 0
# END OF CONFIG
##########################################################################################
def config():
    a = np.loadtxt("Dataset/input1.txt", unpack=True)
    data = np.array(a).tolist()
    global INDIV_SIZE, POP_SIZE, MAX_WEIGHT, PARENT_PERCENT, OFFSPRING_PERCENT, GEN_MAX, CROSSOVER_PROB, MUTATION_PROB, TABU_SIZE, MAX_SIDEWAY, MAX_IMPROVE
    INDIV_SIZE = int(data[0])
    POP_SIZE = int(data[1])
    PARENT_PERCENT = data[2]
    OFFSPRING_PERCENT = data[3]
    GEN_MAX = int(data[4])
    CROSSOVER_PROB = data[5]
    MUTATION_PROB = data[6]
    TABU_SIZE = int(data[7])
    MAX_SIDEWAY = int(data[8])
    MAX_IMPROVE = int(data[9])
        
def initialize():
    a,b = np.loadtxt("ks_20_878", dtype=int, unpack=True)
    values = np.array(a).tolist()
    weights = np.array(b).tolist()
    values.pop(0)
    global MAX_WEIGHT
    MAX_WEIGHT = weights.pop(0)
    for v, w in zip(values, weights):
        ITEMS.append(Item(v,w))

def checkEqual(lst):
   return lst[1:] == lst[:-1]

def fitness(target):
    global INDEX
    INDEX += 1
    total_value = 0
    total_weight = 0
    index = 0
    for i in target:        
        if index >= len(ITEMS):
            break
        if (i == 1):
            total_value += ITEMS[index].value
            total_weight += ITEMS[index].weight
        index += 1
    if total_weight > MAX_WEIGHT:
        return 0
    else:
        return total_value

def spawn_starting_population(amount):
    return [spawn_individual() for x in range (0,amount)]

def spawn_individual():
    if START_POP_WITH_ZEROES:
        mutatedIndividual = mutate([0 for x in range (0,len(ITEMS))])
        return mutatedIndividual
    else:
        return [random.randint(0,1) for x in range (0,len(ITEMS))]

def cdf(probabilities):
    cdfList = []
    indices = [] 
    lastValue = 0
    index = 0
    for p in probabilities:
        lastValue += p
        cdfList.append(lastValue)
    numberOfParents = int(PARENT_PERCENT*POP_SIZE)
    while index < numberOfParents:
        probability = random.uniform(0, 1)
        for idx, val in enumerate(cdfList):
            if probability <= val:
                indices.append(idx)
                index += 1
                break
    return indices

def crossover(population, parentsIndices):
    parents = []
    pairs = []
    child1 = []
    child2 = []
    children = []
    hasOddChildren = False
    for p in parentsIndices:
        parents.append(population[p])
    numberOfChildren = int(OFFSPRING_PERCENT*POP_SIZE)
    if numberOfChildren % 2 != 0:
        hasOddChildren = True
    for p in range(len(parents) // 2 + 1):
        pairs.append((parents[random.randrange(len(parents))], parents[random.randrange(len(parents))]))
    for f,m in pairs:
        probability = random.uniform(0, 1)
        if probability < CROSSOVER_PROB:
            point1 = random.randint(0, INDIV_SIZE-1)
            point2 = random.randint(point1+1, INDIV_SIZE)
            child1 = f[:point1] + m[point1:point2] + f[point2:]
            child2 = m[:point1] + f[point1:point2] + m[point2:]
        else:
            child1 = list(f)
            child2 = list(m)
        child1 = mutate(child1)
        child2 = mutate(child2)
        child1 = local_search(child1)
        child2 = local_search(child2)
        children.append(child1)
        children.append(child2)
    if hasOddChildren == True and len(children):
        children.pop(-1)
    return children

def mutate(individuals):
    probability = random.uniform(0,1)
    for idx, val in enumerate(individuals):
        probability = random.uniform(0,1)
        if probability < MUTATION_PROB:
            if val == 1:
                individuals[idx] = 0
            else:
                individuals[idx] = 1
    return individuals

def find_neighborhood(child):
    bestNeighbor = []
    fitnessMax = 0
    for idx, val in enumerate(child):
        neighbor = list(child)
        if val == 1:
            val = 0
        else:
            val = 1
        neighbor[idx] = val
        neighborFitness = fitness(neighbor)
        if neighborFitness > fitnessMax:
            fitnessMax = neighborFitness
            bestNeighbor = list(neighbor)
    return bestNeighbor, fitnessMax
    


def local_search(child):
    improve = 0
    sideWay = 0
    tabuList = []
    currentNode = list(child)
    while improve < MAX_IMPROVE: 
        bestNeighbor, fitnessMax = find_neighborhood(currentNode)
        nima = fitness(currentNode)
        if fitnessMax > nima:
            sideWay = 0
            tabuList.clear()
            currentNode = list(bestNeighbor)
        elif fitnessMax == nima and currentNode not in tabuList and len(tabuList) < 5 and sideWay < MAX_SIDEWAY:
            tabuList.append(currentNode)
            currentNode = list(bestNeighbor)
            sideWay += 1
        else:
            return currentNode
        improve += 1
    return currentNode
    
def main():
    global INDEX
    config()
    initialize()
    generation = 1
    population = spawn_starting_population(POP_SIZE)
    x = []
    bestFitness = []
    meanFitness = []
    logHeader = ["Best Individual", 'Best Fitness:', 'Mean Fitness:', 'Fitness Function Calls:']
    log = []
    flag = False
    for g in range(0,GEN_MAX):
        fitnessList = []
        print ("Generation %d with %d" % (generation,len(population)))
        # population = sorted(population, key=lambda x: fitness(x), reverse=True)
        for i in population:
            fitnessValue = fitness(i)
            fitnessList.append(fitnessValue)
            print ("%s, fit: %s" % (str(i), fitnessValue))
        if flag:
            break
        if checkEqual(population):
            flag = True
        fitnessArray = np.array(fitnessList)
        finessTotalSum = np.sum(fitnessArray)
        probabilities = fitnessArray / finessTotalSum
        parentsIndices = cdf(probabilities)
        children = crossover(population, parentsIndices)
        population.extend(children)
        population = sorted(population, key=lambda x: fitness(x), reverse=True)
        population = population[:POP_SIZE]
        log.append('Generation ' + str(generation) + ':')
        log.append('\tBest individual: ' + str(population[0]))
        log.append('\tBest Fitness: ' + str(fitness(population[0])))
        meanFitnessValue = finessTotalSum/POP_SIZE
        log.append('\tMean Fitness: ' + str(meanFitnessValue))
        log.append('\tIndex Function Call: ' + str(INDEX) + '\n')

        x.append(generation)
        bestFitness.append(fitness(population[0]))
        meanFitness.append(meanFitnessValue)
        generation += 1
    with open('log.txt', 'w') as f:
        for item in log:
            f.write("%s\n" % item)
    print(INDEX)
    plt.plot(x, bestFitness, color='g')
    plt.plot(x, meanFitness, color='r')
    plt.xlabel('Generation')
    plt.ylabel('Fitness')
    plt.title('Green indicates best fitness\nRed indicated mean fitness')
    plt.show()


if __name__ == "__main__":
    main()
